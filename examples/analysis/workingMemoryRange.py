'''
Analyze the Range Networks
'''
from __future__ import division

import cPickle as pickle
import os
import sys
from   os.path import join

import numpy       as np
import scipy.stats as stats

from pycog          import fittools, RNN, tasktools
from pycog.figtools import Figure, mpl

from pycog.utils import get_here, mkdir_p

THIS = "examples.analysis.workingMemoryDelay"

#=========================================================================================
# Setup
#=========================================================================================

# File to store trials in
def get_trialsfile(p):
    return join(p['trialspath'], p['name'] + '_trials.pkl')

# Load trials
def load_trials(trialsfile):
    with open(trialsfile) as f:
        trials = pickle.load(f)

    return trials, len(trials)

# File to store sorted trials in
def get_sortedfile(p):
    return join(p['trialspath'], p['name'] + '_sorted.pkl')

# Simple choice function
def get_choice(trial):  #return the value of the neuron at the very final time
    return np.argmax(trial['z'][:,-1])

# Define "active" units
def is_active(r):
    return np.std(r) > 0.1

def safe_divide(x):
    if x == 0:
        return 0
    return 1/x

def distribute(p, args):
    from pycog import bsubtools
    f = open('TableData.txt','w').close 
    action = 'trials'
    
    here   = get_here(__file__)
    
    for Range in range(0,5001,500): 
	for network in range(1,6):  
            suffix = str(Range)+'-' + str(network)
            print "Dispatching Network: " + suffix
            jobname = 'Info' + suffix
            cmd     = 'python {} {} run {} {} --Range {} --seed {}'.format(join(here, '../do.py'), 
                  p['modelfile'], join(here, 'workingMemoryRange'), action, Range, network+1045)
            bsubpath = join(p['workpath'], 'bsub')
            jobfile = bsubtools.write_jobfile(cmd, jobname, bsubpath, p['scratchpath'])
            os.system('bsub < ' + jobfile)

def run_trials(p, args):        #args are filename and the number of trials
    f = open('TableData.txt','a')
    
    m = p['model']      # Model
    
    try:
        ntrials = int(args[0])
    except:
        ntrials = 50
    ntrials *= m.nconditions
    
    percentCorrect = [-1 for j in range(0,11)]
    
    networkRange = int(p['Range'])
    if p['seed'] == 100:
	network = 1
    else:
        network = p['seed']-1045
    
    #f.write('delay: {} network: {}\n'.format(networkDelay, network))
    savefile = p['savefile'][:-4] + str(networkRange) + '-' + str(network) + '.pkl'
	#savefile = '/gpfs/home/fas/murray/aba38/pycog/examples/work/data/workingMemoryDelay2/workingMemoryDelay23000-1.pkl'
	#print type(savefile)
    rng = np.random.RandomState(p['seed'])
    rnn = RNN(savefile, {'dt': p['dt']}, verbose=False)	
    for givenDelay in range(0, 5001, 500):
            trial_func = m.generate_trial
            columns = 0
       	    ncorrect = 0
	    correct_by_fpair = {} 
            w = len(str(ntrials))
            trials = []
            backspaces = 0
            try:
                for i in xrange(ntrials):
                    # Condition
                    b      = i % m.nconditions  #iterate through all conditions
                    k0, k1 = tasktools.unravel_index(b, (len(m.pairs), 1))
                    pair  = m.pairs[k0]

                    # Trial
                    trial_args = {
                        'name':  'test',
                        'catch': False,
                        'pair': pair,
                        'givenDelay': givenDelay,
                        }
                    info = rnn.run(inputs=(trial_func, trial_args), rng=rng)

                    # Display trial type
                    s = ("Trial {:>{}}/{}: {:>2} {:>2} on network {} for Range {}"
                         .format(i+1, w, ntrials, info['f1'], info['f2'],networkRange, givenDelay))
                    sys.stdout.write(backspaces*'\b' + s)
                    sys.stdout.flush()
                    backspaces = len(s)

                    # Save
                    dt    = rnn.t[1] - rnn.t[0]
                    step  = int(p['dt_save']/dt)
                    trial = {
                        't':    rnn.t[::step],
                        'u':    rnn.u[:,::step],
                        'r':    rnn.r[:,::step],
                        'z':    rnn.z[:,::step],
                        'info': info
                        }
                    #correct_by_fpair = {}
            
                    # Signed coherence
                    f1 = info['f1']
                    f2 = info['f2']

                    # Correct
                    choice = get_choice(trial)
                    correct_by_fpair.setdefault((f1, f2), []).append(1*(choice == info['choice']))

                    # Overall correct
                    if choice == info['choice']:
                        ncorrect += 1
            
            
            except KeyboardInterrupt:
                pass
            print("")
	    #print correct_by_fpair
	    f.write("{} {} {} {} {} {} {} {}\n".format(networkRange, givenDelay, network, ncorrect*100/ntrials, 
			sum(correct_by_fpair[m.pairs[0]])*m.nconditions*100/ntrials,
			sum(correct_by_fpair[m.pairs[1]])*m.nconditions*100/ntrials, 
			sum(correct_by_fpair[m.pairs[2]])*m.nconditions*100/ntrials, 
			sum(correct_by_fpair[m.pairs[3]])*m.nconditions*100/ntrials))

    f.close()     
     
def do(action, args, p):    #p is params
    
    print("ACTION*:   " + str(action))
    print("ARGS*:     " + str(args))

    
    if action == 'distribute':
        distribute(p, args)
    
    elif action == 'trials':
        run_trials(p, args)
    
    else:
        print("[ {}.do ] Unrecognized action '{}'.".format(THIS, action))
    

