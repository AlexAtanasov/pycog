from __future__ import division
import argparse

import imp

import cPickle as pickle
import os
import sys
from   os.path import join

import numpy       as np
import scipy.stats as stats

from pycog          import fittools, RNN, tasktools
from pycog.figtools import Figure, mpl

def get_choice(trial):  #return the value of the neuron at the very final time
    return np.argmax(trial['z'][:,-1])

def run_trials(p, number, args):        #args are filename and the number of trials
    

    f = open('TableData.txt','w')
    # Model
    m = p['model']

    # Number of trials
    try:
        ntrials = int(args[0])
    except:
        ntrials = 50
    ntrials *= m.nconditions

    percentCorrect = [-1 for j in range(0,11)]
    # RNN
    rows=0
    for networkDelay in range(number,number+1):
	    columns = 0
        savefile = join(datapath, name + 'Delay' + '-' + str(networkDelay) + '.pkl')
        rng = np.random.RandomState(p['seed'])
        rnn = RNN(savefile, {'dt': p['dt']}, verbose=False)
	    for givenDelay in range(0,5500, 500):
            # Trials
	        trial_func = m.generate_trial
            w = len(str(ntrials))
            backspaces = 0
	        ncorrect = 0
            try:
                for i in xrange(ntrials):
                    # Condition
                    b      = i % m.nconditions  #iterate through all conditions
                    k0, k1 = tasktools.unravel_index(b, (len(m.pairs), 1))
                    pair  = m.pairs[k0]

                    # Trial
                    trial_args = {
                        'name':  'test',
                        'catch': False,
                        'pair': pair,
                        'givenDelay': givenDelay,
                        }
                    info = rnn.run(inputs=(trial_func, trial_args), rng=rng)

                    # Display trial type
                    s = ("Trial {:>{}}/{}: {:>2} {:>2} on network {} for delay {}"
                         .format(i+1, w, ntrials, info['f1'], info['f2'],networkDelay, givenDelay))
                    sys.stdout.write(backspaces*'\b' + s)
                    sys.stdout.flush()
                    backspaces = len(s)

                    # Save
                    dt    = rnn.t[1] - rnn.t[0]
                    step  = int(p['dt_save']/dt)
                    trial = {
                        't':    rnn.t[::step],
                        'u':    rnn.u[:,::step],
                        'r':    rnn.r[:,::step],
                        'z':    rnn.z[:,::step],
                        'info': info
                        }
                    #correct_by_fpair = {}
                    
                    # Signed coherence
                    f1 = info['f1']
                    f2 = info['f2']

                    # Correct
                    choice = get_choice(trial)
                    #correct_by_fpair.setdefault((f1, f2), []).append(1*(choice == info['choice']))

                    # Overall correct
                    if choice == info['choice']:
                        ncorrect += 1
                    
                    
            except KeyboardInterrupt:
                pass
            print("")
            
            # WE WILL USE THE CODE BELOW LATER
            # for fpair, correct in correct_by_fpair.items():
            #     correct_by_fpair[fpair] = sum(correct)/len(correct)

            # Report overall performance
	    print 100*ncorrect/ntrials
	    f.write('{} '.format(100*ncorrect/ntrials))
            percentCorrect[columns]=100*ncorrect/ntrials
	    columns += 1
           
        rows += 1 
	f.write('\n')
    return percentCorrect
    
    
   

prefix = 'workingMemory2'

p = argparse.ArgumentParser()
p.add_argument('model_file', help="model specification")
p.add_argument('args', nargs='*')
p.add_argument('-s', '--seed', type=int, default=100)
p.add_argument('--dt', type=float, default=0.5)
p.add_argument('--dt_save', type=float, default=0.5)
p.add_argument('--number', type=int, default=0)

a = p.parse_args()

name = 'workingMemory2'

modelfile = os.path.abspath(a.model_file)
if not modelfile.endswith('.py'):
    modelfile += '.py'
    
#scratchroot = os.environ.get('SCRATCH', join(os.path.expanduser('~'), 'scratch'))
#scratchpath = join(scratchroot, 'work', prefix, name)

here = '/home/fas/murray/aba38/pycog/examples'

workpath = join(here,'work')

#trialspath = join(scratchpath, 'trials')
datapath   = join(workpath, 'data', name)
figspath   = join(workpath, 'figs', name)
    
args    = a.args
seed    = a.seed
dt      = a.dt
dt_save = a.dt_save
number  = a.number

try:
    m = imp.load_source('model', modelfile)
except IOError:
    print("Couldn't load model module from {}".format(modelfile))
    sys.exit()

 
params = {
    'seed':       seed,
    'model':      m,
    'name':       name,
    'datapath':   datapath,
    'figspath':   figspath,
    #'trialspath': trialspath,
    'dt':         dt,
    'dt_save':    dt_save
    }
    
tableData = run_trials(params,number, None)
print tableData
    

