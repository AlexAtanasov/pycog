#BSUB -J variantDelayPlot
#BSUB -q shared
#BSUB -M 8000000
#BSUB -W 24:00
#BSUB -e ~/pycog
#BSUB -e /home/fas/murray/aba38/pycog/examples/analysis/variantDelayPlot.o%J
#BSUB -o /home/fas/murray/aba38/pycog/examples/analysis/variantDelayPlot.o%J

source ~/.bashrc
python variantDelayPlot.py ../models/workingMemory2.py --number 500

exit 0;
