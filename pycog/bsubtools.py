from __future__ import absolute_import

import os

from .utils import mkdir_p

def write_jobfile(cmd, jobname, gsubpath, scratchpath, 
                  nodes=1, ppn=1, gpus=0, mem=8, ndays=1, queue=''):
                      
    if gpus > 0:
        gpus = ':gpus={}'.format(gpus)
    else:
        gpus = ''

    if queue != '':
        queue = '#BSUB -q {}\n'.format(queue)

    if ppn > 1:
        threads = 'export OMP_NUM_THREADS={}\n'.format(ppn)
    else:
        threads = ''

    #print "HERE\n"
    mkdir_p(gsubpath)
    jobfile = os.path.join(gsubpath, jobname + '.sh')
    with open(jobfile, 'w') as f:
        f.write('#! /bin/bash\n'
            + '\n' # + '#BSUB -l nodes={}:ppn={}{}\n'.format(nodes, ppn, gpus)
            + '#BSUB -M {}\n'.format(mem*1000*1000)     #good
            + '#BSUB -W {}:00\n'.format(24*ndays)       #good
            + queue
            + '#BSUB -J {}\n'.format(jobname[0:16])     #good
            + ('#BSUB -e {}/{}.e%J\n' #good
               .format(scratchpath, jobname))
            + ('#BSUB -o {}/{}.o%J\n' #good
               .format(scratchpath,jobname))
            + threads  #good
            + '\n'
            + 'source ~/.bashrc\n'
            + 'cd {}\n'.format(scratchpath) #why did I comment this out?
            + 'pwd > {}.log\n'.format(jobname)  
            + 'date >> {}.log\n'.format(jobname)
            + 'which python >> {}.log\n'.format(jobname)
            + '{} >> {}.log 2>&1\n'.format(cmd, jobname)
            + '\n' 
            + 'exit 0;\n'
            )
    
    return jobfile
    
